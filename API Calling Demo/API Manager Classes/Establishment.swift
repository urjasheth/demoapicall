//
//  Establishment.swift
//  API Calling Demo
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class Establishment {
    var name: String?
    var pricing: String?
    
    func JsonParseFromDict(dict: JSONDictionary) {
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let pricing = dict["pricing"] as? String {
            self.pricing = pricing
        }
    
    }
}
