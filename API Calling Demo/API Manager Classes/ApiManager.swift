//
//  ApiManager.swift
//  OyeDeals
//
//  Created by Ankit on 9/15/17.
//  Copyright © 2017 Solulab. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD
typealias JSONDictionary = [String:AnyObject]

public class ApiManager {
    
    static let shared: ApiManager = ApiManager()
    
    func getEstablishmentResult(establishment:Establishment, view: UIView, completion:@escaping(_ establishment:[Establishment]) -> ()) {

        let str = "http://dev.heat.city/api/establishments?filter={\"limit\":9,\"offset\":0}".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        Alamofire.request(str, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            
            print(response.result)

            if let json = response.result.value as? [JSONDictionary]
            {
                print(json)
            } else {
                UIAlertController.showAlert(withTitle: "Demo", alertMessage: "opps! please try again later", buttonArray: ["Ok"], completion: { (index:Int) in
                })
            }
            MBProgressHUD.hide(for: view, animated: true)
        }
    }
}
import UIKit

class GlobalFunction: NSObject
{
    
    static var instance: GlobalFunction!
    class func sharedInstance() -> GlobalFunction {
        self.instance = (self.instance ?? GlobalFunction())
        return self.instance
    }
    class func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                UIAlertController.showAlert(withTitle: "Oye Deals", alertMessage:"Something went wrong", buttonArray: ["OK"], completion: { (index:Int) in
                    
                })
            }
        }
        return nil
    }
}

extension UIAlertController {
    
    class func showAlert(withTitle alertTitle: String, alertMessage: String, buttonArray: [String], completion: @escaping(_ buttonIndex : Int) -> Void)
    {
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        for i in 0..<buttonArray.count {
            let alertButton = UIAlertAction(title: (buttonArray[i]), style: .default, handler: { UIAlertAction in
                
                completion(i)
                
                alertController.dismiss(animated: true, completion: {
                    
                })
            })
            
            alertController.addAction(alertButton)
        }
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // present the view controller
            topController.present(alertController, animated: true, completion: nil)
            
            
        }
    }
    
}
