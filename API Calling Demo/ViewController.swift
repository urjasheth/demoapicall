//
//  ViewController.swift
//  API Calling Demo
//
//  Created by Hetal Govani on 24/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MBProgressHUD

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getEstablishmentResult()
    }

    func getEstablishmentResult() {
        let establishment = Establishment()

        MBProgressHUD.showAdded(to: self.view, animated: true)
        ApiManager.shared.getEstablishmentResult(establishment: establishment, view: self.view) { (result : [Establishment]) in
    
            print(result)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

